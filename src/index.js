import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// [section] JSX (JavaScript +XML)
  // it is an extension of JS that let us create objects which then be compiled and added as HTML elements.
  // with JSX, we are able to create HTML elements using JS.
  // Without using the following metod:
  //  .value - elements and .innerHTML - div, p, span methods
  // JSX provides "syntactic sugar" for creating react components
    // syntax within a programming language that is designed to make things easier to read and or to express.
    // <Home/>

// const name = "Christopher Mortel";
// const element = <h1>Hello, {name}</h1>;

// root.render(element);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

