
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home"
import Profile from "./pages/Profile";


function App() {

  let profileProp = {
    firstName: "John",
    lastName: "Doe",
    email: "john.doe@mail.com",
    mobileNo: "09123456789",
    isAdmin: false
    }


  return ( 
        <Profile data = {profileProp}/>
  
        );
}

export default App;
