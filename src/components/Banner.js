import { Row, Button, Col } from "react-bootstrap";

// Pass-by-value (parameter passing )
export default function Banner({data}){

    const { title, content, label} = data;



    return(
        <Row>
		    <Col className="p-5 m-5 text-center">
		        <h1>{title}</h1>
		        <p>{content}</p>
		        <Button>{label}</Button>
		    </Col>
		</Row>
    )
}