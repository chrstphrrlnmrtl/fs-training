import {Row, Col, Button} from "react-bootstrap"


export default function Profile({data}){
    

        const { firstName, lastName, email, mobileNo, isAdmin} = data;
    return(
      
        (data.isAdmin)
        ?
            <Row>
		      <Col className="p-5 m-5 text-center">
		        <h1>Welcome, {firstName} {lastName}</h1>
		        <Button>Check Dashboard</Button>
		      </Col>
		    </Row> 
       
        :
            <Row>
		      <Col className="p-5 m-5 text-center">
		        <h1>Welcome, {firstName} {lastName}</h1>
            <h3>Contact:</h3>
		        <p>email: {email} | Mobile No. : {mobileNo}</p>
		        <Button>Edit Account</Button>
		      </Col>
		    </Row> 
      
        
        
        
      
        
    )
}